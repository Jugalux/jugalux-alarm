import React, { useState, useEffect } from "react"
import { View, Text } from 'react-native'
import { Audio } from 'expo-av'


const Alarm = () => {

    const [sound, setSound] = useState<Audio.Sound | null>(null)
  
    useEffect(() => {
        async function loadSound() {
            const { sound } = await Audio.Sound.createAsync(require('../media/alarms/mixkit.wav'))
            setSound(sound)
        }
        loadSound()
    }, [])
  
    async function playSound() {
        await sound?.playAsync()
    }
  
    return (
      <View>
        <Text onPress={playSound}>Jouer le son</Text>
      </View>
    )
}

export default Alarm