import React, { useState, useEffect } from "react"
import { Audio } from 'expo-av'


export function alertAtDateTime(date: Date, time: Date): boolean {

    // State of sound ringer
	const [sound, setSound] = useState<Audio.Sound | null>(null)

    // Play sound function
	useEffect(() => {
        async function loadSound() {
            const { sound } = await Audio.Sound.createAsync(require('../media/alarms/mixkit.wav'))
            setSound(sound)
        }
        loadSound()
    }, [])
  
    async function playSound() {
        await sound?.playAsync()
    }

    //get date info
    const targetDateTime = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      time.getHours(),
      time.getMinutes(),
      time.getSeconds()
    )
  
    const checkDateTime = () => {
        const now = new Date()
        if(now >= targetDateTime) {
            console.log("L'heure est arrivée !")
            playSound()
            return true
        }
        setTimeout(checkDateTime, 1000)

        return false
    }
  
    return checkDateTime()
}  