import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import DateTimePickerModal from "react-native-modal-datetime-picker"
import { parse } from 'date-fns'
import Alarm from './components/Alarm'
import { alertAtDateTime } from './components/Functions'


export default function App() {

	// States of date & time pickers
	const [isDatePickerVisible, setIsDatePickerVisible] = useState(false)
	const [isTimePickerVisible, setIsTimePickerVisible] = useState(false)
	const [selectedDate, setSelectedDate] = useState('')
	const [selectedTime, setSelectedTime] = useState('')

	// Play Sound when date and time arrived
	const dateTimestamp = Date.parse(selectedDate)
	const parsedTime = parse(selectedTime, "hh:mm:ss a", new Date())
	const dateObj = new Date(dateTimestamp)
	const timeObj = new Date(parsedTime)
	const isDefinedTime = alertAtDateTime( dateObj, timeObj)

	useEffect(() => {
		const intervalId = setInterval(() => {
			isDefinedTime ? console.log('It is time') : null
		}, 1000)
	  
		return () => {
			clearInterval(intervalId)
		}
	}, [isDefinedTime])

	// Pickers date and time functions
	const showDatePicker = () => {
		setIsDatePickerVisible(true)
	}

	const hideDatePicker = () => {
		setIsDatePickerVisible(false)
	}

	const handleDateConfirm = (date : Date) => {
		setSelectedDate(date.toDateString())
		hideDatePicker()
	}

	const showTimePicker = () => {
		setIsTimePickerVisible(true)
	}

	const hideTimePicker = () => {
		setIsTimePickerVisible(false)
	}

	const handleTimeConfirm = (time: Date) => {
		setSelectedTime(time.toLocaleTimeString())
		hideTimePicker()
	}

	const setAlarm = () => {
		console.log(`Alarme enregistré pour ${selectedDate} à ${selectedTime}`)
	}

	return (
		<View style={styles.container}>
			<Text style={styles.title}>Jugalux Alarm</Text>

			<TouchableOpacity onPress={showDatePicker} style={styles.button}>
				<Text style={styles.buttonText}>Sélectionner une date</Text>
			</TouchableOpacity>

			{selectedDate ? (
				<Text style={styles.selectedDate}>{selectedDate}</Text>
			) : null}

			<TouchableOpacity onPress={showTimePicker} style={styles.button}>
				<Text style={styles.buttonText}>Sélectionner l'heure</Text>
			</TouchableOpacity>

			{selectedTime ? (
				<Text style={styles.selectedTime}>{selectedTime}</Text>
			) : null}

			<TouchableOpacity onPress={setAlarm} style={styles.button}>
				<Text style={styles.buttonText}>Définir une alarme</Text>
			</TouchableOpacity>

			<DateTimePickerModal
				isVisible={isDatePickerVisible}
				mode="date"
				onConfirm={handleDateConfirm}
				onCancel={hideDatePicker}
				maximumDate={new Date(2080, 10, 20)}
				minimumDate={new Date()}
				themeVariant='dark'
				positiveButton={{label: 'Enregistrer la date', textColor: 'green'}}
				negativeButton={{label: 'Retour', textColor: '#5c0f8a'}}
				style={{ backgroundColor: '#5c0f8a' }}
			/>

			<DateTimePickerModal
				isVisible={isTimePickerVisible}
				mode="time"
				onConfirm={handleTimeConfirm}
				onCancel={hideTimePicker}
			/>

			<Alarm />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 32,
		marginBottom: 20,
	},
	button: {

	},
	buttonText: {

	},
	selectedTime: {

	},
	selectedDate: {

	}
})
